# HomeTask
A python application exposing a single generic endpoint capable of filtering, grouping and sorting a sample dataset.


## Installation
1. Start up your terminal (or Command Prompt on Windows OS).
2. Ensure that you've `python` installed on your PC.
3. Clone the repository.
4. Navigate to the project folder using `cd into the project folder` on your terminal (or command prompt)
5. After cloning, create a virtual environment then install the requirements with the command:
`pip install -r requirements.txt`.
6. Create a `.env` file in your root directory as described in `.env.sample` file.

7. After creating the `.env`, Setup up your database following these steps: 
    `python manage.py wait_for_db && python manage.py migrate && python manage.py runserver`

## Urls for testcases
1. http://localhost:8000/core/performance-metrics/?ordering=-clicks&date__lte=2017-06-01&sum=impressions,clicks&group=channel,country
2. http://localhost:8000/core/performance-metrics/?ordering=date&date__gt=2017-04-30&date__lt=2017-06-01&os=ios&sum=installs&group=date
3. http://localhost:8000/core/performance-metrics/?ordering=-revenue&date=2017-06-01&country=US&sum=revenue&group=os
4. http://localhost:8000/core/performance-metrics/?ordering=-cpi&country=CA&group=channel&cpi=true

