from django.db.models import Sum, F, FloatField, ExpressionWrapper
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import FilterSet

from .models import PerformanceMetrics
from .serializers import PerformanceMetricSerializer


class PerformanceMetricFilter(FilterSet):
    class Meta:
        model = PerformanceMetrics
        fields = {
            'channel': ['exact'],
            'country': ['exact'],
            'os': ['exact'],
            'date': ['exact', 'lt', 'gt', 'lte', 'gte']
        }


class PerformanceMetricList(generics.ListAPIView):
    """List data from performnce metrics table"""
    queryset = PerformanceMetrics.objects.all()
    serializer_class = PerformanceMetricSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering_fields = '__all__'
    filterset_class = PerformanceMetricFilter

    def _get_fields(self, query_param_type):
        fields = self.request.query_params.get(query_param_type, None)
        if fields:
            fields = fields.split(',')
        return fields

    def _build_fields_to_return(self):
        fields_to_return = []
        for key in ['sum', 'group']:
            fields = self._get_fields(key)
            if fields:
                fields_to_return = fields_to_return + fields

        if self.request.query_params.get('cpi'):
            fields_to_return.append('cpi')

        return list(set(fields_to_return))

    def get_queryset(self):
        queryset = PerformanceMetrics.objects.all()
        sum_fields = self._get_fields('sum')
        group_fields = self._get_fields('group')
        cpi = self.request.query_params.get('cpi')

        if group_fields:
            queryset = queryset.values(*group_fields)
        if sum_fields:
            queryset = queryset.annotate(
                **{value: Sum(value) for value in sum_fields}
            )
        if cpi == 'true':
            queryset = queryset.annotate(cpi=ExpressionWrapper(
                F('spend') / F('installs'),
                output_field=FloatField()
            ))

        return queryset

    def get(self, request, format=None):
        default_fields = ['id', 'country', 'channel', 'date', 'os',
                          'impressions', 'clicks', 'spend', 'revenue']
        queryset = self.get_queryset()
        filtered_queryset = self.filter_queryset(queryset)
        # import pdb; pdb.set_trace()
        fields_to_return = self._build_fields_to_return() or default_fields
        serializer = self.get_serializer(
            filtered_queryset, fields=fields_to_return, many=True
        )
        return Response(serializer.data)
