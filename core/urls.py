from django.urls import path

from .views import PerformanceMetricList

urlpatterns = [
    path('performance-metrics/', PerformanceMetricList.as_view())
]
