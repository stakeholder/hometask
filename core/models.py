from django.db import models


class PerformanceMetrics(models.Model):
    date = models.DateField()
    channel = models.CharField(max_length=255)
    country = models.CharField(max_length=2)
    os = models.CharField(max_length=20)
    impressions = models.IntegerField()
    clicks = models.IntegerField()
    installs = models.IntegerField()
    spend = models.DecimalField(max_digits=10, decimal_places=2)
    revenue = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = 'performance_metrics'
