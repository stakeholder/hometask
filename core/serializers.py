from rest_framework import serializers

from .models import PerformanceMetrics


class PerformanceMetricSerializer(serializers.ModelSerializer):
    cpi = serializers.FloatField()

    class Meta:
        model = PerformanceMetrics
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        super().__init__(*args, **kwargs)

        if fields is not None:
            allowed = set(fields)
            exisiting = set(self.fields)
            for field_name in exisiting - allowed:
                self.fields.pop(field_name)
